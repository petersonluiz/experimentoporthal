/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.porthal.experimento.resources;

import br.com.porthal.experimento.ejb.ConfiguracaoFinanceiroSession;
import br.com.porthal.experimento.entity.ConfiguracaoFinanceiro;
import java.math.BigDecimal;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.hibernate.service.Service;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author douglas giordano
 */
public class ConfigFinanceiroResourceTest extends JerseyTest {

    @Mock
    private ConfiguracaoFinanceiroSession applicationService;

    public ConfigFinanceiroResourceTest() {
    }

    @Override
    protected Application configure() {
        MockitoAnnotations.initMocks(this);
    return new ResourceConfig()
        .register(ConfigFinanceiroResource.class)
        .register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(applicationService).to(ConfiguracaoFinanceiroSession.class);
            }
        });
    }

    @Test
    public void testGet() {
//        System.out.println("get");
//        ConfigFinanceiroResource instance = new ConfigFinanceiroResource();
//        String expResult = "";
//        String result = instance.get();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
    }

    @Test
    public void testPost() {
        ConfiguracaoFinanceiro config = new ConfiguracaoFinanceiro();
        config.setCalcularJuro(true);
        config.setCalcularMulta(true);
        config.setToleranciaAtrasoSemJuro(30);
        config.setToleranciaAtrasoSemMulta(25);
        config.setId(2);
        config.setJuroAtraso(new BigDecimal(2));
        config.setMultaAtraso(new BigDecimal(2));
        final Boolean salvou = target("/configuracaofinanceiro")
                .request()
                .post(Entity.xml(config), Boolean.class);

        assertEquals(true, salvou);
    }

}
