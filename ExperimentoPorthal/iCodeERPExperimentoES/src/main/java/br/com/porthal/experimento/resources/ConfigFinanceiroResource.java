/*
 */
package br.com.porthal.experimento.resources;

import br.com.porthal.experimento.ejb.ConfiguracaoFinanceiroSession;
import br.com.porthal.experimento.entity.ConfiguracaoFinanceiro;
import br.com.porthal.experimento.parse.Parser;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;

/**
 */
@Path("/configuracaofinanceiro")
@ApplicationScoped
public class ConfigFinanceiroResource {

    @EJB
    private ConfiguracaoFinanceiroSession configFinanceiroSession;

    public ConfigFinanceiroResource() {
        try {
            String lookupName = "java:global/iCodeERPExperimentoES-1.0-SNAPSHOT/ConfiguracaoFinanceiroSession";
            configFinanceiroSession = (ConfiguracaoFinanceiroSession) InitialContext.doLookup(lookupName);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_XML)
    public String get() {
        return Parser.getConfigFinanceiro(getConfigFinanceiroSession().getConfiguracao());
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public boolean post(ConfiguracaoFinanceiro c) {
        try {
            this.getConfigFinanceiroSession().save(c);
            return true;
        } catch (Exception ex) {//implementar tratamento
            return false;
        }
    }

    /**
     * @return the configFinanceiroSession
     */
    public ConfiguracaoFinanceiroSession getConfigFinanceiroSession() {
        return configFinanceiroSession;
    }

    /**
     * @param configFinanceiroSession the configFinanceiroSession to set
     */
    public void setConfigFinanceiroSession(ConfiguracaoFinanceiroSession configFinanceiroSession) {
        this.configFinanceiroSession = configFinanceiroSession;
    }
}
