/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.porthal.experimento.entity;

import br.com.porthal.experimento.persistence.MyInterfaceEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Porthal
 */
@Entity
@Table(name = "CONTA")
public class Conta implements Serializable, MyInterfaceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_CONTA")
    protected Integer id;

    @NotNull
    @Column(name = "DATA_EMISSAO")
    @Temporal(TemporalType.DATE)
    protected Date dataEmissao;

    @NotNull
    @Column(name = "DATA_VENCIMENTO")
    @Temporal(TemporalType.DATE)
    protected Date dataVencimento;

    @Column(name = "NUMERO_DUPLICATA")
    protected int numeroDuplicata;

    @Column(name = "DUPLICATA_PAGA")
    protected boolean duplicataPaga;

    @NotNull
    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID")
    @ManyToOne
    protected Cliente cliente;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTA_COMPRA",
            joinColumns = {
                @JoinColumn(name = "ID_CONTA")},
            inverseJoinColumns = {
                @JoinColumn(name = "ID_COMPRA")})
    protected List<Compra> historicoCompra;

    @NotNull
    @Column(name = "VALOR_DUPLICATA")
    protected BigDecimal valorDuplicata;
    @NotNull
    @Column(name = "VALOR_MULTA")
    protected BigDecimal valorMulta;
    @NotNull
    @Column(name = "VALOR_JUROS")
    protected BigDecimal valorJuros;
    @NotNull
    @Column(name = "VALOR_TOTAL")
    protected BigDecimal valorTotal;
    @NotNull
    @Column(name = "VALOR_PENDENTE")
    protected BigDecimal valorPendente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the dataEmissao
     */
    public Date getDataEmissao() {
        return dataEmissao;
    }

    /**
     * @param dataEmissao the dataEmissao to set
     */
    public boolean setDataEmissao(Date dataEmissao) {
        if (!dataEmissao.after(dataVencimento)) {

            this.dataEmissao = dataEmissao;
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return the dataVencimento
     */
    public Date getDataVencimento() {
        return dataVencimento;
    }

    /**
     * @param dataVencimento the dataVencimento to set
     */
    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    /**
     * @return the numeroDuplicata
     */
    public int getNumeroDuplicata() {
        return numeroDuplicata;
    }

    /**
     * @param numeroDuplicata the numeroDuplicata to set
     */
    public void setNumeroDuplicata(int numeroDuplicata) {
        this.numeroDuplicata = numeroDuplicata;
    }

    /**
     * @return the duplicataPaga
     */
    public boolean isDuplicataPaga() {
        return duplicataPaga;
    }

    /**
     * @param duplicataPaga the duplicataPaga to set
     */
    public void setDuplicataPaga(boolean duplicataPaga) {
        this.duplicataPaga = duplicataPaga;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the historicoCompra
     */
    public List<Compra> getHistoricoCompra() {
        return historicoCompra;
    }

    /**
     * @param historicoCompra the historicoCompra to set
     */
    public void setHistoricoCompra(List<Compra> historicoCompra) {
        this.historicoCompra = historicoCompra;
    }

    /**
     * @return the valorDuplicata
     */
    public BigDecimal getValorDuplicata() {
        return valorDuplicata;
    }

    /**
     * @param valorDuplicata the valorDuplicata to set
     */
    public void setValorDuplicata(BigDecimal valorDuplicata) {
        this.valorDuplicata = valorDuplicata;
        setValorPendente(valorDuplicata);
    }

    /**
     * @return the valorMulta
     */
    public BigDecimal getValorMulta() {
        return valorMulta;
    }

    /**
     * @param valorMulta the valorMulta to set
     */
    public void setValorMulta(BigDecimal valorMulta) {
        this.valorMulta = valorMulta;
    }

    /**
     * @return the valorJuros
     */
    public BigDecimal getValorJuros() {
        return valorJuros;
    }

    /**
     * @param valorJuros the valorJuros to set
     */
    public void setValorJuros(BigDecimal valorJuros) {
        this.valorJuros = valorJuros;
    }

    /**
     * @return the valorTotal
     */
    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    /**
     * @return the valorPendente
     */
    public BigDecimal getValorPendente() {
        return valorPendente;
    }

    /**
     * @param valorPendente the valorPendente to set
     */
    private void setValorPendente(BigDecimal valorPendente) {
        this.valorPendente = valorPendente;
    }

}
