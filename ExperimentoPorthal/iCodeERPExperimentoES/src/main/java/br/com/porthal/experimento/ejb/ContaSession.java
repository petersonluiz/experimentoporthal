/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.porthal.experimento.ejb;

import br.com.porthal.experimento.entity.Cliente;
import br.com.porthal.experimento.entity.ConfiguracaoFinanceiro;
import br.com.porthal.experimento.entity.Conta;
import br.com.porthal.experimento.persistence.NewPersistence;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ecar. S. M.
 */
@Stateless
@LocalBean
public class ContaSession extends NewPersistence<Conta, Integer> {

    //<editor-fold defaultstate="collapsed" desc="INIT">
    @PersistenceContext(unitName = "iCodeERPExperimentoPU", name = "iCodeERPExperimentoPU", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Override
    @PostConstruct
    public void init() {
        this.object = new Conta();
    }

    @Override
    public Conta getObject() {
        return this.object;
    }

    @Override
    public EntityManager getEntityManager() {
        return this.entityManager;
    }
    //</editor-fold>

    @EJB
    private ConfiguracaoFinanceiroSession configSession;

    @Override
    public Conta save(Conta conta) {
        return super.save(calcularValores(conta, this.configSession.getConfiguracao()));
    }

    public Conta consultarConta(Integer numeroDuplicata, Cliente cliente) {
        return calcularValores(findConta(numeroDuplicata, cliente), this.configSession.getConfiguracao());
    }
    
    @Override
    public boolean remove(Conta conta) {
        if(!conta.isDuplicataPaga()) {
            return super.remove(conta);
        } else {
            return false;
        }
    }
    
    public Conta edit(Conta conta) {
//        Conta conta = consultarConta(numeroDuplicata, cliente);
        if(!conta.isDuplicataPaga()) {
            return save(conta);
        } else {
            return null;
        }
    }

    private Conta findConta(Integer numeroDuplicata, Cliente cliente) {
        
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Conta> criteria = builder.createQuery(Conta.class);
        Root<Conta> root = criteria.from(Conta.class);

        criteria.where(builder.equal(root.get("numeroDuplicata"), numeroDuplicata));
        criteria.where(builder.equal(root.get("cliente"), cliente));

        try {
            return getEntityManager().createQuery(criteria).getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            return null;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="CALC">
    private Integer calculaDiasAtraso(Date dataAtual, Date dataVencimento) {
        Calendar calendarAtual = Calendar.getInstance();
        calendarAtual.setTime(dataAtual);
        Calendar calendarVencimento = Calendar.getInstance();
        calendarVencimento.setTime(dataVencimento);

        int diferenca = (calendarVencimento.get(Calendar.YEAR) * 12 + calendarVencimento.get(Calendar.MONTH))
                - (calendarAtual.get(Calendar.YEAR) * 12 + calendarAtual.get(Calendar.MONTH));

        return diferenca;
    }

    private BigDecimal calculaMulta(BigDecimal valorPendente, BigDecimal multaAtraso) {
        return valorPendente.multiply(multaAtraso.divideToIntegralValue(BigDecimal.valueOf(100.0)));
    }

    private BigDecimal calculaJuros(BigDecimal valorPendente, BigDecimal jurosAtraso, Integer diasAtraso) {
        return valorPendente.multiply(jurosAtraso.divide(BigDecimal.valueOf(100.0).divide(BigDecimal.valueOf(30.0)))).multiply(BigDecimal.valueOf(diasAtraso).movePointLeft(2));
    }

    private BigDecimal calculaTotal(BigDecimal valorPendente, BigDecimal valorJuros, BigDecimal valorMulta) {
        return valorPendente.add(valorJuros).add(valorMulta);
    }

    private Conta calcularValores(Conta conta, ConfiguracaoFinanceiro config) {
        Integer diasAtraso = calculaDiasAtraso(new Date(), conta.getDataVencimento());
        if (config.isCalcularMulta()) {
            if (diasAtraso > config.getToleranciaAtrasoSemMulta()) {
                conta.setValorMulta(calculaMulta(conta.getValorPendente(), config.getMultaAtraso()));
            }
        }

        if (config.isCalcularJuro()) {
            if (diasAtraso > config.getToleranciaAtrasoSemJuro()) {
                conta.setValorJuros(calculaJuros(conta.getValorPendente(), config.getJuroAtraso(), diasAtraso));
            }
        }

        conta.setValorTotal(calculaTotal(conta.getValorPendente(), conta.getValorJuros(), conta.getValorMulta()));

        return conta;
    }
    //</editor-fold>

}
