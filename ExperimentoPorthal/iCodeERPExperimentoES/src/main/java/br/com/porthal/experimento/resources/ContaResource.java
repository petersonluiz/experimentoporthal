/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.porthal.experimento.resources;

import br.com.porthal.experimento.ejb.ContaSession;
import br.com.porthal.experimento.entity.Cliente;
import br.com.porthal.experimento.entity.Conta;
import br.com.porthal.experimento.parse.Parser;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Porthal
 */
@Path("/conta")
public class ContaResource {

    @EJB
    private ContaSession contaSession;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_XML)
    public String consultarConta(int numeroDuplicata, Cliente cliente) {
        return Parser.getConta(this.contaSession.consultarConta(numeroDuplicata, cliente));
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public boolean inserirConta(Conta c) {
        try {
            this.contaSession.save(c);
            return true;
        } catch (Exception ex) {//implementar tratamento
            return false;
        }
    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public boolean editarConta(Conta c) {
        try {
            this.contaSession.edit(c);
            return true;
        } catch (Exception ex) {//implementar tratamento
            return false;
        }
    }

    @DELETE
    @Path("/")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public boolean excluirConta(Conta c) {
        try {
            this.contaSession.remove(c);
            return true;
        } catch (Exception ex) {//implementar tratamento
            return false;
        }
    }
}
